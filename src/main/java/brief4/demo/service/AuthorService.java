package brief4.demo.service;

import brief4.demo.entity.Author;
import brief4.demo.repository.AuthorRepository;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class AuthorService {
    private final AuthorRepository authorRepository;

    public AuthorService(AuthorRepository authorRepository) {
        this.authorRepository = authorRepository;
    }
    public Author postAuthor(Author model) {
        return authorRepository.save(model);
    }
    public Iterable <Author> getAuthor() {
        return authorRepository.findAll();
    }
    public Optional<Author> findById(Long id) {
        return
                authorRepository.findById(id);
    }


}