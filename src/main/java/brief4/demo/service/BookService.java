package brief4.demo.service;

import brief4.demo.entity.Book;
import brief4.demo.repository.BookRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Service
public class BookService {

    private final BookRepository bookRepository;

    public BookService(BookRepository bookRepository) {
        this.bookRepository = bookRepository;
    }


    public Book saveBook(Book book) {
        return bookRepository.save(book);
    }


    public List<Book> fetchBookList() {
        return (List<Book>)
                bookRepository.findAll();
    }

    public Book findByTitle(String title) {
        return
                bookRepository.findByTitle(title);
    }

    public Optional<Book> getBookById(long id) {
        return
                bookRepository.findById(id);
    }


    public Book
    updateBook(Book book, Long bookId) {
        Book bookDB = bookRepository.findById(bookId).get();
        if (Objects.nonNull(book.getTitle())
                && !"".equalsIgnoreCase(
                book.getTitle())) {
            bookDB.setTitle(
                    book.getTitle());
        }
        if (Objects.nonNull(book.isAvailable())) {
            bookDB.setAvailable(book.isAvailable());
        }
        if (Objects.nonNull(
                book.getDescription())
                && !"".equalsIgnoreCase(
                book.getDescription())) {
            bookDB.setDescription(
                    book.getDescription());
        }
        if (Objects.nonNull(
                book.getCategory())
                && !"".equalsIgnoreCase(
                book.getCategory().getName())) {
            bookDB.setCategory(
                    book.getCategory());
        }
        return bookRepository.save(bookDB);
    }

    public void deleteBookById(Long bookId) {
        bookRepository.deleteById(bookId);
    }

}
