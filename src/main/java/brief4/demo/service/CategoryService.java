package brief4.demo.service;

import brief4.demo.entity.Category;
import brief4.demo.repository.CategoryRepository;
import org.springframework.stereotype.Service;

@Service
public class CategoryService {
    private CategoryRepository categoryRepository;

    public CategoryService(CategoryRepository categoryRepository) {
        this.categoryRepository = categoryRepository;
    }

    public Category saveCategory(Category model) {

        return categoryRepository.save(model);
    }

    public Iterable<Category> getCategory() {
        return categoryRepository.findAll();
    }
}
