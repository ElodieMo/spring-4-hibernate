package brief4.demo.controller;

import brief4.demo.entity.Author;
import brief4.demo.service.AuthorService;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
public class AuthorController {
    private final AuthorService authorService;

    public AuthorController(AuthorService authorService) {
        this.authorService = authorService;
    }

    @PostMapping("/authors")
    public Author postAuthor(@RequestBody Author model) {
        return authorService.postAuthor(model);
    }

    @GetMapping("/authors")

    public Iterable<Author> getAuthor(){return authorService.getAuthor();}

    @GetMapping("/authors/{authorId}/books")
    @ResponseBody
    public Author findById(@PathVariable(name = "authorId")Long id) {
        Optional<Author> author = authorService.findById(id);
        if (author.isPresent()) {
            return author.get();
        }
        return null;
    }
}