package brief4.demo.controller;

import brief4.demo.entity.Book;
import brief4.demo.entity.Category;
import brief4.demo.service.CategoryService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class CategoryController {
    private final CategoryService categoryService;

    public CategoryController(CategoryService categoryService) {
        this.categoryService = categoryService;
    }

    @PostMapping("/category")
    public Category saveCategory(
            @RequestBody Category category) {
        return categoryService.saveCategory(category);
    }
    @GetMapping("/category")
    public Iterable<Category> getCategory() {
        return categoryService.getCategory();

    }
}
