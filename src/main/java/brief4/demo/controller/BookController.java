package brief4.demo.controller;

import brief4.demo.entity.Book;
import brief4.demo.service.BookService;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;


@RestController
public class BookController {


    private final BookService bookService;

    public BookController(BookService bookService) {
        this.bookService = bookService;
    }

    @PostMapping("/books")
    public Book saveBook(
            @RequestBody Book book) {
        return bookService.saveBook(book);
    }

    @GetMapping("/books")
    public List<Book> fechBookList() {
        return bookService.fetchBookList();

    }

    @GetMapping("/books/{id}")
    @ResponseBody
    public Book getBookById(@PathVariable Long id) {
        Optional<Book> book = bookService.getBookById(id);
        if (book.isPresent()) {
            return book.get();
        }
        return null;
    }

    @GetMapping("/books/title/{title}")
    @ResponseBody
    public Book findByTitle(@PathVariable String title) {

        return bookService.findByTitle(title);
    }


    @PutMapping("/books/{id}")
    public Book updateBook(@RequestBody Book book,
                           @PathVariable("id") Long bookId) {
        return bookService.updateBook(
                book, bookId);
    }

    @DeleteMapping("/books/{id}")
    public String deleteBookById(@PathVariable("id")
                                 Long bookId) {
        bookService.deleteBookById(
                bookId);
        return "Deleted Successfully";
    }
}
