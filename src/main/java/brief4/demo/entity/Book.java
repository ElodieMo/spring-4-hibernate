package brief4.demo.entity;

import jakarta.persistence.*;
import org.springframework.lang.Nullable;

import java.util.ArrayList;
import java.util.List;

@Entity
public class Book {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    @Column(nullable = false)
    private String title;

    @Lob
    @Nullable
    @Column(columnDefinition = "longtext")
    private String description;

    @Column(nullable = false, columnDefinition = "boolean default true")
    private Boolean available;

    @ManyToOne
    @JoinColumn(name ="category_id")
    private Category category;
    @ManyToMany
    @JoinColumn(name ="author_id")
    private List<Author> authorList= new ArrayList<>();

    public Boolean getAvailable() {
        return available;
    }

    public void setAvailable(Boolean available) {
        this.available = available;
    }

    public Book() {

    }

    public Boolean isAvailable() {
        return available;
    }

    public void setAvailable(boolean available) {
        this.available = available;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Nullable
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
